#!/usr/bin/python3
import sys

def main():
    info_hash_string = sys.argv[1]
    info_hash = bytes.fromhex(info_hash_string)


    '''
    info_hash = ""
    i = 2
    for c in info_hash_string:
        if (i == 2):
            i = 0
            info_hash += "\\x"
        i += 1
        info_hash += c
    '''
    fd = open("info_hash", "wb")
    fd.write(bytearray(info_hash))
    fd.close()
    return

if __name__ == "__main__":
    main()

