#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>


char *protocol_id = "\x00\x00\x04\x17\x27\x10\x19\x80";
char *connect_action = "\x00\x00\x00\x00";
char *announce_action = "\x00\x00\x00\x01";
char *transaction_id = "\xF1\xE2\xD3\xC4";
char connection_id[8];
char info_hash[20];
char *peer_id = "AAAABBBBCCCCDDDDEEEE";
char *downloaded = "\x00\x00\x00\x00\x00\x00\x00\x00";
char *left = "\x00\x00\x00\x00\x00\x00\x00\x00";
char *uploaded = "\x00\x00\x00\x00\x00\x00\x00\x00";
char *event = "\x00\x00\x00\x0";
char *ip_address = "\x00\x00\x00\x00";
char *key = "\xDE\xAD\xBE\xEF";
//char *num_want = "\xFF\xFF\xFF\xFF";
char *num_want = "\x0F\xFF\xFF\xFF";
char *port_num = "\xD9\x03";


#define PORT 6969


int main(int argc, char **argv)
{
	int fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		printf("error\n");
		exit(-1);
	}
	char ih[20];
	read(fd, ih, 20);
	close(fd);
	memcpy(info_hash, ih, 20);
	//info_hash = "\x96\xEB\xC2\xC1\xB3\x2D\x0C\xD0\x8E\x0F\x45\xD8\x52\x3A\x2D\xDE\x5C\xB9\xA4\x49";
	struct sockaddr_in serv_addr;
	int sockfd;
	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket");
		exit(-1);
	}

	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	serv_addr.sin_addr.s_addr = inet_addr("45.154.253.10");

	int n, len;
	char connect_request[16];

	memcpy(connect_request, protocol_id, 8);
	memcpy(connect_request + 8, connect_action, 4);
	memcpy(connect_request + 12, transaction_id, 4);

	sendto(sockfd, (const char *)connect_request, 16, 0, (const struct sockaddr *)&serv_addr, sizeof(serv_addr));
	//printf("Initial UDP packet sent\n");

	char buf[4096];
	n = recvfrom(sockfd, (char *)buf, 4096, MSG_WAITALL, (struct sockaddr *)&serv_addr, &len);
	buf[n] = 0;
	//printf("Response received\n");
	//printf("Bytes received: %i\n", n);
	//printf("Response: %s\n", buf);

	if (n > 8) 
		memcpy(connection_id, buf + 8, 8);
	
	// announcement_request size:
	// 8 + 4 + 4 + 20 + 20 + 8 + 8 + 8 + 4 + 4 + 4 + 4 + 2 = 98
	
		

	char announcement_request[98];

	memcpy(announcement_request, connection_id, 8);
	memcpy(announcement_request + 8, announce_action, 4);
	memcpy(announcement_request + 12, transaction_id, 4);
	memcpy(announcement_request + 16, info_hash, 20);
	memcpy(announcement_request + 36, peer_id, 20);
	memcpy(announcement_request + 56, downloaded, 8);
	memcpy(announcement_request + 64, left, 8);
	memcpy(announcement_request + 72, uploaded, 8);
	memcpy(announcement_request + 80, event, 4);
	memcpy(announcement_request + 84, ip_address, 4);
	memcpy(announcement_request + 88, key, 4);
	memcpy(announcement_request + 92, num_want, 4);
	memcpy(announcement_request + 96, port_num, 2);

	memset(buf, 0, 4096);

	sendto(sockfd, (const char *)announcement_request, 98, 0, (const struct sockaddr *)&serv_addr, sizeof(serv_addr));
	n = recvfrom(sockfd, (char *)buf, 4096, MSG_WAITALL, (struct sockaddr *)&serv_addr, &len);
	buf[n] = 0;
	//printf("Response received\n");
	//printf("Bytes received: %i\n", n);
	//printf("Response: %s\n", buf);

	char **ip_list = (char **)malloc(sizeof(char *) * 0xFFFF);
	uint16_t *port_list = (uint16_t *)malloc(sizeof(uint16_t *) * 0xFFFF);

	for (int i = 0; i < 4096; i++) 
		ip_list[i] = (char *)malloc(INET_ADDRSTRLEN);

	uint32_t leechers = ntohl(*(uint32_t *)(buf + 12));
	uint32_t seeders = ntohl(*(uint32_t *)(buf + 16));
	char *addrs = buf + 20;
	int num_records = (n - 20) / 6;
	uint32_t temp;
	char *tempstr;
	struct in_addr ip_addr;
	for (int i = 0; i < num_records; i++) {
		temp = *(uint32_t *)(addrs + 6*i);
		ip_addr.s_addr = temp;
		tempstr = inet_ntoa(ip_addr);
		memcpy(ip_list[i], tempstr, strlen(tempstr)) ;
		port_list[i] = ntohs(*(uint16_t *)(buf + 4 + 6*i)); 
		printf("%s\n", ip_list[i]);
		//printf("%s:%u\n", ip_list[i], port_list[i]);
	}
	
		


	close(sockfd);

	return 0;
}
